#!/usr/bin/env python3


# dunder
if __name__ == '__main__':
	# What is different here
	print('Hello World')
	print("Hello World")

	# Won't do anything
	1+1

	'''
	Multiline comment
	'''

	print('''
	Multiline
	print
	Hello world
	''')

	print(__name__)
	print(__file__)
