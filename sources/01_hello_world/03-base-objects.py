#!/usr/bin/env python3.7
# Content of this file:


# list, set, ... and loops
# for else

# comprehensions

# dict iterations
# items, key, values
# change dict when iterating over it

# string formatting

# function definitions - more later - mypy etc later,
# lambdas
# map, filter



# But Why?
if __name__ == '__main__':
	pass # syntactic sugar to define empty block


if __name__ == '__main__':
	# lists
	list_a = ['a', 'b', 'c']
	list_a = list('abc') # element in list for each element in iterable

	list_a.append('d')
	list_a.extend(['e', 'f', 'g'])

	list_a = list_a + ['h', 'i', 'j']

	print(list_a)


	# tuples
	tuple_a = ('a', 'b', 'c',)
	tuple_a = ('a', 'b', 'c')
	tuple_a = tuple('abc')

	tuple_b = ('a')  # beware, not a tuple!
	tuple_c = ('a',) # this is a tuple

	# Immutable!, low memory footprint, fast
	print(tuple_a)
	tuple_a = tuple_a + ('d', 'e', 'f',)
	print(tuple_a)

	# constructing and destructing
	(a, b) = (1, 2)
	print(a)
	print(b)


	# dictionaries = maps
	# order do not match insertation
	#	- depends on implementation (CPython >3.5 follows order)
	dict_a = {
		'a': 'b',
		'b': 1,
		'c': ['d'],
	}
	# Upper is better, see dict_b
	dict_a = dict(
		a = 'b',
		b = 1,
		c = ['d'],
	)
	print(dict_a)
	print(dict_a['a'])

	dict_a.update({'a': 'a', 'd': 'd'})
	print(dict_a)

	# Any hashable structure can be key
	dict_b = {
		(1, 2,): 'a',
	}
	print(dict_b)


	# sets
	# Unordered same as dict
	set_a = {'a', 'b', 'c'}
	set_a = set('abc')
	# equivalent to: (but with friendlier API)
	dict_set = {'a': None, 'b': None, 'c': None}

	# https://docs.python.org/3.7/library/stdtypes.html#set-types-set-frozenset
	print(set_a)
	print(set_a & {'b', 'c'})


	# loops
	# no index, concepts of iterables
	for element in list_a:
		print(element)


	# destructing
	for (index, element) in enumerate(list_a):
		print(index, element)

	# Works without destructing too
	for element in enumerate(list_a):
		# this is tuple
		print(element)

	# for..else
	for (index, element) in enumerate(list_a):
		print(index, element)
		# if index == 2:
		# 	break
	else:
		print('Done')


	# comprehensions
	# build list of `Item 1`, ...

	list_example = []
	for index in range(10):
		list_example.append('Item ' + str(index))
		# Calling method on string - everything is object (even strings, ints, ...)
		# list_example.append('Item {}'.format(index))
		# list_example.append(f'Item {index}')

	print(list_example)

	list_example = [f'Item {index}' for index in range(10)]
	print(list_example)

	# works for others (but tuple)
	dict_example = {
		str(index): f'Item {index}'
		for index in range(10)
	}

	set_example = {f'Item {index}' for index in range(10)}

	# beware this
	not_tuple_example = (f'Item {index}' for index in range(10))
	print(not_tuple_example)

	tuple_example = tuple(f'Item {index}' for index in range(10))



	# missed dict for loops
	dict_a = dict(
		a = 'b',
		b = 1,
		c = ['d'],
	)
	for item in dict_a:
		print(item)

	# Again other collections are objects too
	for key, value in dict_a.items():
		print(key, value)

	for key in dict_a.keys(): # .values()
		print(key)

	# beware changing dicts *SIZE* when iterating over them
	# for key, value in dict_a.items():
	# 	dict_a['d'] = 'a'
	# make copy? change how your program work?


	# Helper functions
	# map, filter, ...

	list_example = [f'Item {index}' for index in range(10)]
	# filter number greater then 3

	for element in list_example:
		if int(element.split(' ')[-1]) > 3:
			print(element)


	# function definition
	def number_filter(element):
		return int(element.split(' ')[-1]) > 3

	filtered_list = list(filter(number_filter, list_example))
	print(filtered_list)

	# Lambda definition - another way how to define function
	mapped_list = list(map(lambda element: int(element.split(' ')[-1]), list_example))
	print(mapped_list)
