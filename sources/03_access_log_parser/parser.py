#!/usr/bin/env python3.7
import io
import click



@click.command()
@click.argument('filename', type = click.File(mode = 'r'))
def main(filename: io.TextIOWrapper) -> None:
	'''
	How to use this
	'''
	print(type(filename))
	# After access is done, move this to __main__


if __name__ == '__main__':
	# pylint don't understand click decorators
	main() # pylint: disable=no-value-for-parameter
