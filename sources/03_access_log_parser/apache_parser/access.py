# Tell about __init__
# tell about self
# methods are bound functions
import datetime
import re
import dataclasses

import dateutil.parser


REGEX = re.compile(
	r'(?P<source_ip>[(\d\.)]+) - - \[(?P<datetime>.*?)\] "(?P<http>.*?)" (?P<response_code>\d+) '
	r'((\d+)|-) "(.*?)" "(?P<user_agent>.*?)" "(.*?)"'
)



@dataclasses.dataclass()
# Maybe show dataclasses later
class Record:

	# Tell about slots later on
	# describe how inner __dict__ working
	# show it in ipython if needed
	__slots__ = (
		'source_ip',
		'event_datetime',
		'method',
		'path',
		'response_code',
		'user_agent',
	)

	source_ip: str
	event_datetime: datetime.datetime
	method: str
	path: str
	response_code: int
	user_agent: str


	# def __init__(
	# 		self,
	# 		source_ip: str,
	# 		event_datetime: datetime.datetime,
	# 		method: str,
	# 		path: str,
	# 		response_code: int,
	# 		user_agent: str,
	# ) -> None:
	# 	self.source_ip = source_ip
	# 	self.event_datetime = event_datetime
	# 	self.method = method
	# 	self.path = path
	# 	self.response_code = response_code
	# 	self.user_agent = user_agent



def parse(line: str) -> Record:
	match = REGEX.match(line.strip())
	if match is None:
		raise ValueError(f'Line is not proper access log entry = {line}')

	groups = match.groupdict()
	method, path, _ = groups['http'].split(' ')
	return Record(
		source_ip = groups['source_ip'],
		event_datetime = dateutil.parser.parse(groups['datetime'].replace(':', ' ', 1)),
		method = method,
		path = path,
		response_code = int(groups['response_code']),
		user_agent = groups['user_agent']
	)


# Show what happens when missing if `__name__` ....
