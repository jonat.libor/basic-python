#!/usr/bin/env python3.7
import io
import click

import apache_parser.access



@click.command()
@click.argument('filename', type = click.File(mode = 'r'))
def main(filename: io.TextIOWrapper) -> None:
	'''
	How to use this
	'''
	records = map(
		apache_parser.access.parse,
		filter(lambda v: bool(v.strip()), filename)
	)

	datetimes = map(
		lambda v: v.event_datetime,
		records
	)

	# why slow?
	# Using regex for each line - but compiled!
	# formatting datetime for each line, try timeit
	#
	# import dateutil.parser
	# %timeit 'text="12/Dec/2015:18:25:11 +0100"' dateutil.parser.parse(text.replace(':', ' ', 1))
	#
	# import apache_parser.access
	# In [12]: %timeit apache_parser.access.parse(line)
	# 124 µs ± 1.58 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)
	#
	print(min(datetimes))


if __name__ == '__main__':
	# pylint don't understand click decorators
	main() # pylint: disable=no-value-for-parameter
