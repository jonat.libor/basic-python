import datetime

from .. import access



def test_parser() -> None:
	line = '109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" ' \
		   '"Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-"'
	record = access.parse(line)

	assert record.source_ip == '109.169.248.247'
	assert record.method == 'GET'
	# Not best way how to work with timezones
	assert record.event_datetime == datetime.datetime(
		2015, 12, 12, 18, 25, 11,
		tzinfo = datetime.timezone(datetime.timedelta(hours = 1))
	)
