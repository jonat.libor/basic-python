#!/usr/bin/env python3.7
from typing import Iterable, Any
import sys
import logging


# NOTES:
#  - Definovani funkci
#    - Funkcionalni pristup - ne vse musi byt objekt
#    - Testovatelnost, purefunkce - vnitrni stav, ...
def count_elements(elements: Iterable[Any]) -> int:
	return sum(map(lambda _: 1, elements))


def file_lines(filename: str) -> int:
	# beware naming it `file` - shadows default build in
	with open(filename, 'r') as file_handler:
		return count_elements(file_handler)


# Talk about exceptions and way let python die
# Radeji skoncit s chybou nez ji tise ignorovat
if __name__ == '__main__':
	logging.basicConfig(
		handlers = [logging.StreamHandler(sys.stderr)],
		level = logging.DEBUG,
	)
	logger = logging.getLogger('__main__')

	# would not catch it if this was
	# an utility tool for me - exception is better then some words
	filename = sys.argv[1]
	try:
		result = file_lines(filename)
	# Handle file not found here - for user only
	except FileNotFoundError:
		logger.error(f'File = %s not found!', filename)
		raise
	except IndexError:
		logger.error(f'Use %s <filename>', sys.argv[0])
		# Exit code, or use exit(1)
		raise
	else:
		print(f'{result} {filename}')
