from typing import Iterable, Any
import py._path.local
import pytest

import wc



def test_count() -> None:
	data = list(range(5))
	assert wc.count_elements(data) == 5


# tell about type: ignore
@pytest.mark.parametrize('data,expected', [ # type: ignore
	(list(range(5)), 5),
	({str(e) for e in range(10)}, 10),
])
def test_parametrize(data: Iterable[Any], expected: int) -> None:
	assert wc.count_elements(data) == expected


def test_file(tmpdir: py._path.local.LocalPath) -> None: # pylint: disable=protected-access
	# https://docs.pytest.org/en/2.8.7/tmpdir.html
	file_handler = tmpdir.mkdir('sub_dir').join('hello.txt')

	file_handler.write(
		'\n'.join([
			'Hello'
			for _ in range(100)
		])
	)

	# Tell about --cov / test coverage
	assert wc.file_lines(str(file_handler)) == 100
