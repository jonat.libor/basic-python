# Word count example

  1. Create virtualenv

```
virtualenv -p $(which python3.7) env
```

  2. Install packages

```
pip install -r requirements.txt -r dev-requirements.txt
```

## Run it

**tests:** `pytest -vvv -s tests/`
**linter:** `pylint wc.py`
**type checker:** `mypy --strict wc.py`
