Celá přednáška bude s otevřeným editorem (budeme vysvětlovat jazyk tak aby bylo rovnou videt jak funguje.)
Takže místo klasického výkladu a ukázek mimo context, si půjdeme hned něco naprogramovat a ukazovat si nové věci podle toho jak je budeme potřebovat.
Zároveň bude interaktivní a budu přizpůsobovat podle toho co kdo zná.


- Co je Python
  - Různé verze Pythonu (2/3)
  - Různé implementace Pythonu
  - Jak Python "spustit"
    - Python interpreter (spustit Python jako intepreter)
  - K čemu je Python dobrý:
  	 - Scripty, je na "každém" Linuxu (dnes si ukzeme pouze tuto oblast - zaklady)
  	 - Webové služby
  	 - Vědecké a analyticke nástroje, machine-learning
  	 - Stock trading
     - ...
  - IDE (vim, pycharm), Jupyter Notebook, Lab, iPython
  - Doporučená literatura


- První program (Hello World)
  - Jednoduchý `print`
  - Vylepšení - více "Pythonic way"
  
  - Cykly
	- Listy, N-tice, slovniky, mnoziny
	- Konstrukce for..else
	- List, dict, set comprehention, pozor na tuple comprehention
	 - Operace nad jednotlivými objekty
	 - Funkce pro iterables
  - Všechno je objekt, int, string, ...

- Jednoduchý word-count (WC) program (functions, packages, virtualenv, ...)
  - Definovaní funkcí
  	- Funkcionalní přístup
  		- Testovatelnost, pure-funkce - vnitrni stav, ...
    - Argumenty, args, kwargs
  - Práce se soubory
  - Vyjímky 
  	- Konstrukce try..else, finally, except, bare except
  	- Radeji skoncit s chybou než ji tiše ignorovat
  - Zápis výsledku zpět do souboru
  - Práce s balíčky
  	- Existují i jiné než pip
  	- Nejde nam nic nainstalovat => virtualenv, virtualenv wrappers
      - Nepoužívat pip pod rootem
  - Pytest - náš první test
  - Pylint - nechme si náš výtvor překontrolovat

- Apache access log parser (classes, ...) http://www.almhuette-raith.at/apache-log/access.log
  => Složitější aplikace na které, si ukážeme použití vlastních modulů a tříd
  - První modul
  	- jak funguje import, __init__
  	- PYTHONPATH
  - První třída
  	- __init__
  	- self
  	- metody
  	- class, static methods
  	- privátní attributy a metody
      - Pozor na počet vnitřních stavů
  - Reprezentace jedné řádky access logu
    - regex
  	- slots
  	- Namapování třídy na obsah souboru
  - Třída reprezentující access soubor
  	- Min, max => Práce s Python datetime

- Pokud zbyde čas 
  - Dekorátory
  - Generátory
